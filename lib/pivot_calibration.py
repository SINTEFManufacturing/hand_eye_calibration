# coding=utf-8

"""
A standard tool-tip calibration procedure found in most robot controllers is based on pivot calibration. Assume a tool tip point Ft fixed in the tool flange, F. Sample a series of flange poses in base coordinates, BFi, where t is kept also fixed with respect to the robot base frame, B, such that Bt is also a fixed point. Then for all i,j we have BFi * Ft = Bt = BFj * Ft. Formulating BFi = [Ri, ti] and casting the equation as Ax=b we can obtain a stacked system of equations for some set of indices {(i,j)}_i!=j:
[Ri - Rj]      [tj - ti]
[  ...  ] Ft = [  ...  ]
[  ...  ]      [  ...  ]
(Note the order of indices on the opposing sides!)

We can isolate for Ft, and then estimate Bt as the average over {BFi * Ft}_i.

This method is described as "Algebraic Two Step" in the publication

@inproceedings{yaniv2015pivot,
  title={Which pivot calibration?},
  author={Yaniv, Ziv},
  booktitle={Medical Imaging 2015: Image-Guided Procedures, Robotic Interventions, and Modeling},
  volume={9415},
  pages={941527},
  year={2015},
  organization={International Society for Optics and Photonics}
}

"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2019-2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


from itertools import combinations

import numpy as np
import math3d as m3d


class PivotCalibrator:

    def __init__(self, BFs: list[m3d.Transform]):
        self._BFs = BFs
        self._combs = list(combinations(self._BFs, 2))
        # print(list(self._combs))

    def __call__(self):
        A = np.vstack([BFi.orient.array - BFj.orient.array
                       for (BFi, BFj) in self._combs])
        # print(A)
        # print(self._combs[0][0].pos)
        dBFps = [(BFj.pos.array - BFi.pos.array).reshape((3,1))
                 for (BFi, BFj) in self._combs]
        b = np.vstack(dBFps)
        Ft = np.linalg.pinv(A).dot(b).reshape(-1)
        # print(ft)
        self.Ft = m3d.PositionVector(Ft)
        # print(self.ft)
        Bt = np.average(
            [(BFi * self.Ft).array.reshape(-1)
             for BFi in self._BFs], axis=0)
        # print(bt)
        self.Bt = m3d.PositionVector(Bt)
        return self.Ft, self.Bt
