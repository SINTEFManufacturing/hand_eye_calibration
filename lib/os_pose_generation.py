# coding=utf-8

"""
Routine for generating sensor pose in calibration object reference.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import typing
import dataclasses

import math3d as m3d
import numpy as np


class Limits(typing.NamedTuple):
    low: np.float64
    high: np.float64


@dataclasses.dataclass
class OSGeneratorLimits:
    yaw: Limits
    pitch: Limits
    roll: Limits
    retract: Limits
    dyaw: Limits
    dpitch: Limits


@dataclasses.dataclass
class OSPoseGen:
    """The process of generating a random sensor in (calibration) object
    pose consists of a series of operations with intrinsic (mobile)
    reference 1) initialize to base, 2) yaw around x, 3) pitch around
    y, 4) roll around z, 5) retract along z, 6) small yaw around
    x, and 7) small pitch around y.
    """
    OS_base: m3d.Transform
    osg_lims: OSGeneratorLimits

    def __call__(self):
        # Start at base
        OS = self.OS_base.copy()
        OS.orient.rotate_xt(np.random.uniform(*self.osg_lims.yaw))
        OS.orient.rotate_yt(np.random.uniform(*self.osg_lims.pitch))
        OS.orient.rotate_zt(np.random.uniform(*self.osg_lims.roll))
        OS.pos -= np.random.uniform(*self.osg_lims.retract) * OS.orient.vec_z
        OS.orient.rotate_xt(np.random.uniform(*self.osg_lims.dyaw))
        OS.orient.rotate_yt(np.random.uniform(*self.osg_lims.dpitch))
        return OS

