# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import numpy as np
import math3d as m3d

import hand_eye_calibration.pivot_calibration as pvc


def test(n=5, noise=0.005):
    """Test and return the identification errors for a generated set of
    poses and applying a given noise to the synthetic ft when
    generating poses.
    """
    Bt = m3d.PositionVector(0.5, 0.6, 0.7)
    Ft = m3d.PositionVector(0.1, 0.05, 0.15)
    # Generate random orientation Assume limited ergodicity
    BFs = []
    for i in range(n):
        BFo = m3d.Orientation.new_euler(np.random.uniform(0, 1, 3))
        BFp = Bt - BFo * m3d.FreeVector(Ft.array + np.random.uniform(-noise, noise, 3))
        BFs.append(m3d.Transform(BFo, BFp))
    pc = pvc.PivotCalibrator(BFs)
    pc()
    return ((pc.Ft - Ft).length, (pc.Bt - Bt).length)
    # return Bt, Ft, pc
    

def test_identify(range_=(3, 10), rep=100, noise=0.005):
    """Plot the error in identifying ft for a given range of poses, with a
    number of repetitions for every pose number and a given noise.
    """
    import matplotlib.pyplot as plt
    err = []
    for n in range(*range_):
        for i in range(rep):
            err.append((n, test(n, noise)[0]))
    plt.scatter(*np.array(err).T, marker='+')
    plt.title(f'Ft error, noise={noise}, repetitions={rep}')
    plt.xlabel('Number of samples')
    plt.ylabel('Error per experiment')
    plt.show()


test_identify()
