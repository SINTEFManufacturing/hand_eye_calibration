# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF Manufacturing 2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"


import numpy as np
import math3d as m3d
import math3d.visualization as m3dv

import hand_eye_calibration.cs_pose_generation as csp


def test():
    tv = m3dv.TransformVisualizer(uvec_length=0.5)
    cspg = csp.CSPoseGen(
        CS_base=m3d.Transform(
            m3d.Orientation.new_rot_y(np.pi),
            m3d.PositionVector()),
        csg_lims=csp.CSGeneratorLimits(
            yaw=csp.Limits(0, np.pi/4),
            pitch=csp.Limits(-np.pi/4, np.pi/4),
            roll=csp.Limits(-np.pi/6, np.pi/6),
            retract=csp.Limits(0.5, 1.0),
            dyaw=csp.Limits(-np.pi/15, np.pi/15),
            dpitch=csp.Limits(-np.pi/12, np.pi/12)))
    tv.plot(cspg.CS_base, uvec_length=0.25, label='CS_base')
    for i in range(10):
        tv.plot(cspg(),
                uvec_length=0.1,
                label=str(i))
    tv.show()


test()
