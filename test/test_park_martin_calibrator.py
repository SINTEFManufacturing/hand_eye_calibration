# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "Morten Lind, SINTEF Manufacturing 2021"
__credits__ = ["Morten Lind"]
__license__ = "LGPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten@lind.fairuse.org, morten.lind@sintef.no"
__status__ = "Development"


import numpy as np
import math3d as m3d

import hand_eye_calibration as hec
import hand_eye_calibration.cs_pose_generation as csg


# Sensor in flange pose
FS_n = m3d.Transform(m3d.RotationVector(np.pi/3, np.pi, np.pi/6),
                   m3d.PositionVector(0.01, -0.05, 0.1))

# Calibration object in base pose
BC = m3d.Transform(m3d.RotationVector(0, 0, np.pi/4),
                   m3d.PositionVector(0.5, -0.1, 0.3))

# Sensor in calibration object poses
cs_gen = csg.CSPoseGen(
    CS_base=m3d.Transform(
            m3d.Orientation.new_rot_y(np.pi),
            m3d.PositionVector()),
    csg_lims=csg.CSGeneratorLimits(
            yaw=csg.Limits(0, np.pi/4),
            pitch=csg.Limits(-np.pi/4, np.pi/4),
            roll=csg.Limits(-np.pi/6, np.pi/6),
            retract=csg.Limits(0.5, 1.0),
            dyaw=csg.Limits(-np.pi/15, np.pi/15),
            dpitch=csg.Limits(-np.pi/12, np.pi/12)))

# Noise level for position and orientation
noise = 0.001
# Number of pose pair samples
N = 10

# Generate perfect poses
CSs = [cs_gen() for i in range(N)]
BFs = [BC * CS * FS_n.inverse for CS in CSs]

# Add noise
for BF in BFs:
    BF.pos += m3d.FreeVector(np.random.uniform(-noise, noise, 3))
    BF.orient *= m3d.Orientation(m3d.RotationVector(np.random.uniform(-noise, noise, 3)))

# Set up for calibration
pmc = hec.ParkMartinCalibrator(pose_pairs=list(zip(BFs, CSs)))

# Get calibrated sensor in flange
FS_c = pmc.sensor_in_flange

# Print error
print(FS_c.pos.dist(FS_n.pos), FS_c.orient.ang_dist(FS_n.orient))
